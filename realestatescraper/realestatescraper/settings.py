# -*- coding: utf-8 -*-

# Scrapy settings for realestatescraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'realestatescraper'

SPIDER_MODULES = ['realestatescraper.spiders']
NEWSPIDER_MODULE = 'realestatescraper.spiders'

ITEM_PIPELINES = {
     #TODO: 'realestatescraper.pipelines.DeDuplicatesPipeline': 300,
    'realestatescraper.pipelines.JsonWriterPipeline': 800,
}
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'realestatescraper (+http://www.yourdomain.com)'
