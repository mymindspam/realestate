# -*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from realestatescraper.items import PropertyItem

class RealestateSpider(CrawlSpider):
    name = "realestate"
    allowed_domains = ["realestate.com.au"]

    def __init__(self, *args, **kwargs):
        super(CrawlSpider, self).__init__(*args, **kwargs)
        self.start_urls = [ "http://www.realestate.com.au/buy/list-1" ]
        self._rules = (
            Rule(LinkExtractor(allow=('/property-') ), callback=self.parse_item),
        )

    def parse_item(self, response):
        article = response
        yield PropertyItem(
            price=article.xpath(".//p[@class='priceText']/text()").extract()[0],
            adress=article.xpath(".//span[@itemprop='streetAddress']/text()").extract()[0],
            phone=article.xpath(".//li[contains(class, phone)]/a/@data-value").extract()[0],
        )